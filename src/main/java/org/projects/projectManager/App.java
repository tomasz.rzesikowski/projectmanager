package org.projects.projectManager;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.projects.projectManager.entity.CloseableEntityManagerFactory;
import org.projects.projectManager.repository.RepositoryControl;
import org.projects.projectManager.controller.BasicWindowController;

import javax.persistence.Persistence;
import java.io.IOException;

/**
 * JavaFX App
 */
public class App extends Application {

    private static Scene scene;
    private static RepositoryControl rc;

    @Override
    public void start(Stage stage) throws IOException {
        scene = new Scene(loadFXML("BasicWindow"));
        stage.setScene(scene);
        stage.show();
    }

    static void setRoot(String fxml) throws IOException {
        scene.setRoot(loadFXML(fxml));
    }

    private static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource(fxml + ".fxml"));
        fxmlLoader.setController(new BasicWindowController(rc));
        return fxmlLoader.load();
    }

    public static void main(String[] args) {
        rc = new RepositoryControl(
                new CloseableEntityManagerFactory(Persistence.createEntityManagerFactory("managerPu"))
        );
        new DBPopulator(rc);
        launch();
    }

}
