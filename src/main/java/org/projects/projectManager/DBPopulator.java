package org.projects.projectManager;

import org.projects.projectManager.model.*;
import org.projects.projectManager.repository.*;

public class DBPopulator {
    public DBPopulator(RepositoryControl rc) {

        User user = UserBuilder.anUser()
                .withInitials("TRZ")
                .withName("Tomasz")
                .withSurname("Tomasz")
                .build();
        rc.getUserRepository().create(user);

        Ship ship = ShipBuilder.aShip()
                .withShipNumber(123)
                .withShipName("Test")
                .build();
        rc.getShipRepository().create(ship);

        ship = ShipBuilder.aShip()
                .withShipNumber(13)
                .withShipName("Test")
                .build();
        rc.getShipRepository().create(ship);

        Room room = RoomBuilder.aRoom()
                .withRoomNumber("IV T 0 T")
                .withCorrespondentDrawing("corresponded")
                .withShip(ship)
                .build();
        rc.getRoomRepository().create(room);

        Order order = OrderBuilder.anOrder()
                .withOrderNumber(123)
                .withOrderDescription("test")
                .withShip(ship)
                .build();
        rc.getOrderRepository().create(order);

        Section section = SectionBuilder.aSection()
                .withGroupNumber(1190)
                .withDrawingNumber("21.56")
                .withTitle("Section 1")
                .withRoom(room)
                .withOrder(order)
                .build();
        rc.getSectionRepository().create(section);

        order = OrderBuilder.anOrder()
                .withOrderNumber(12783)
                .withOrderDescription("test")
                .withShip(ship)
                .build();
        rc.getOrderRepository().create(order);

        section = SectionBuilder.aSection()
                .withGroupNumber(1190)
                .withDrawingNumber("21.56")
                .withTitle("Section 2")
                .withRoom(room)
                .withOrder(order)
                .build();
        rc.getSectionRepository().create(section);



        section = SectionBuilder.aSection()
                .withGroupNumber(1190)
                .withDrawingNumber("21.56")
                .withTitle("Section 3")
                .withRoom(room)
                .withOrder(order)
                .build();
        rc.getSectionRepository().create(section);



    }
}
