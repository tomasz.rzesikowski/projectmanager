package org.projects.projectManager.controller;

import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import org.projects.projectManager.model.Section;
import org.projects.projectManager.model.Ship;
import org.projects.projectManager.model.User;
import org.projects.projectManager.repository.RepositoryControl;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class BasicWindowController implements Initializable {

    private final RepositoryControl rc;

    @FXML
    private BorderPane mainWindowPane;

    @FXML
    private ComboBox<Integer> selectShip;

    @FXML
    private TabPane tabs;

    //Ship table view

    @FXML
    private TableView<Ship> shipsTable;

    @FXML
    private TableColumn<Ship, String> shipNumberColumn;

    @FXML
    private TableColumn<Ship, String> shipNameColumn;
    //Section table view

    @FXML
    private TableView<Section> sectionsTable;
    //end

    @FXML
    private TableColumn<Section, String> shipNumberSectionsColumn;

    @FXML
    private TableColumn<Section, String> groupNumberSectionsColumn;

    @FXML
    private TableColumn<Section, String> drawingNumberSectionsColumn;

    @FXML
    private TableColumn<Section, String> titleSectionsColumn;

    @FXML
    private TableColumn<Section, String> roomSectionsColumn;

    //User table view
    @FXML
    private TableView<User> usersTable;
    //end

    @FXML
    private TableColumn<User, String> userInitialsColumn;

    @FXML
    private TableColumn<User, String> userFullNameColumn;

    @FXML
    private TableColumn<User, Integer> userPrivilegeColumn;

    public BasicWindowController(RepositoryControl rc) {
        this.rc = rc;
    }
    //end

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        update();
    }

    @FXML
    private void newShipDialog() {
        WindowsFXMLLoader.loadDialog(mainWindowPane, "NewShipDialog", new NewShipDialogController(rc, this));
    }

    @FXML
    private void newOrderDialog() {
        WindowsFXMLLoader.loadDialog(mainWindowPane, "NewOrderDialog", new NewOrderDialogController(rc));
    }

    @FXML
    private void newUserDialog() {
        WindowsFXMLLoader.loadDialog(mainWindowPane, "NewUserDialog", new NewUserDialogController(rc));
        update();
    }

    @FXML
    private void newSectionDialog() {
        WindowsFXMLLoader.loadDialog(mainWindowPane, "NewSectionDialog", new NewSectionDialogController(rc));
        update();
    }

    @FXML
    private void importFromCSV() {
        WindowsFXMLLoader.loadDialog(mainWindowPane, "ImportDialog", new ImportDialogController(rc));
        update();
    }

    @FXML
    private void close() {
        Platform.exit();
    }

    @FXML
    private void shipSelected(ActionEvent event) {
        if (selectShip.getValue() != null) {
            createTabs(selectShip.getValue());
            }
    }


    private void createTabs(Integer shipNumber) {
        tabs.getTabs().clear();
        rc.getOrderRepository()
                .findAllWithinShip(shipNumber)
                .forEach(order -> tabs.getTabs().add(creteOrderTabWithSections(shipNumber, order.getOrderId().getOrderNumber())));
    }

    private Tab creteOrderTabWithSections(Integer shipNumber, Integer orderNumber) {
        Tab orderTab = new Tab(orderNumber.toString());
        orderTab.setContent(createSectionTable(shipNumber, orderNumber));
        return orderTab;
    }

    private TableView<Section> createSectionTable(Integer shipNumber, Integer orderNumber) {

        List<Section> sections = rc.getOrderRepository().findAllSectionsWithShipAndOrder(shipNumber, orderNumber);

        ObservableList<Section> observableSections = javafx.collections.FXCollections.observableArrayList(sections);

        TableView<Section> sectionsTable = new TableView<>(observableSections);

        TableColumn<Section, Integer> shipNumberColumn = new TableColumn<>("Statek");
        shipNumberColumn.setCellValueFactory(new PropertyValueFactory<>("shipNumber"));

        TableColumn<Section, String> sectionNumberColumn = new TableColumn<>("Numer sekcji");
        sectionNumberColumn.setCellValueFactory(new PropertyValueFactory<>("sectionNumber"));

        TableColumn<Section, String>  titleSectionColumn = new TableColumn<>("Tytuł sekcji");
        titleSectionColumn.setCellValueFactory(new PropertyValueFactory<>("title"));

        TableColumn<Section, String>  roomSectionColumn = new TableColumn<>("Pomieszczenie");
        roomSectionColumn.setCellValueFactory(new PropertyValueFactory<>("room"));

        sectionsTable.getColumns().addAll(shipNumberColumn, sectionNumberColumn, titleSectionColumn, roomSectionColumn);

        sectionsTable.autosize();
        return sectionsTable;
    }

    protected void update() {
        selectShip.getItems().clear();
        selectShip.getItems().addAll(rc.getShipRepository().findAllShipsNumbers());
    }

}

