package org.projects.projectManager.controller;

import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DialogPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import org.apache.commons.csv.CSVRecord;
import org.projects.projectManager.model.*;
import org.projects.projectManager.repository.RepositoryControl;

import java.net.URL;
import java.util.*;
import java.util.stream.Collectors;


public class ImportDialogController extends ViewController implements Initializable {
    private final RepositoryControl rc;

    public ImportDialogController(RepositoryControl rc) {
        this.rc = rc;
    }
    List<Order> orders = new ArrayList<>();

    @FXML
    private DialogPane dialogPane;

    @FXML
    private ComboBox<Integer> shipNumber;

    @FXML
    private TextField pathToFile;

    @FXML
    private ComboBox<Integer> orderNumbers;

    @FXML
    private Text statusText;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        shipNumber.getItems().addAll(rc.getShipRepository().findAllShipsNumbers());
    }

    @FXML
    public void shipIDSelected() {
        orderNumbers.getSelectionModel().clearSelection();
        orderNumbers.getItems().clear();
        orders = rc.getOrderRepository().findAllWithinShip(shipNumber.getSelectionModel().getSelectedItem());

        for (Order order : orders) {
            orderNumbers.getItems().add(order.getOrderId().getOrderNumber());
        }
    }


    @Override
    public void onOK(ActionEvent event) {
        Ship selectedShip = rc.getShipRepository().find(shipNumber.getSelectionModel().getSelectedItem());
        Order selectedOrder = rc.getOrderRepository().find(new OrderId(orderNumbers.getSelectionModel().getSelectedItem(), selectedShip));


        List<CSVRecord> records = InputController.loadSectionsFromFile(pathToFile.getText());
        generateRooms(records, selectedShip);

        for (CSVRecord record: records) {
            Section section = SectionBuilder.aSection()
                    .withGroupNumber(Integer.valueOf(record.get("group")))
                    .withDrawingNumber(record.get("section"))
                    .withTitle(record.get("title"))
                    .withOrder(selectedOrder)
                    .withRoom(rc.getRoomRepository().find(new RoomId(record.get("room"), selectedShip)))
                    .build();
            rc.getSectionRepository().create(section);
        }
        onCancel(event);
    }

    private void generateRooms(List<CSVRecord> records, Ship ship) {
        List<Room> rooms = rc.getRoomRepository().findAll();
        for (CSVRecord record : records) {
            if (rooms.stream().noneMatch(room -> room.getRoomId().getRoomNumber().equals(record.get("room")))) {
                Room room = RoomBuilder.aRoom().withRoomNumber(record.get("room")).withShip(ship).build();
                rooms.add(room);
                rc.getRoomRepository().create(room);
            }
        }
    }
}
