package org.projects.projectManager.controller;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.projects.projectManager.model.*;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

public class InputController {
    private static final String[] sectionHeader = {"group", "section", "title", "room"};

    public static List<CSVRecord> loadSectionsFromFile (String patchToFile) {
        List<Section> sections = new ArrayList<>();
        List<CSVRecord> records = new ArrayList<>();
        try (Reader in = new FileReader(patchToFile)) {
            records = CSVFormat.DEFAULT
                    .withHeader(sectionHeader)
                    .withFirstRecordAsHeader()
                    .parse(in).getRecords();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return records;
    }
}
