package org.projects.projectManager.controller;

import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import org.projects.projectManager.model.Order;
import org.projects.projectManager.model.OrderBuilder;
import org.projects.projectManager.model.Ship;
import org.projects.projectManager.repository.RepositoryControl;

import java.net.URL;
import java.util.ResourceBundle;


public class NewOrderDialogController extends ViewController implements Initializable {
    private final RepositoryControl rc;

    public NewOrderDialogController(RepositoryControl rc) {
        this.rc = rc;
    }

    @FXML
    private ComboBox<Integer> shipNumber;

    @FXML
    private TextField orderNumber;

    @FXML
    private TextField orderDescription;

    @FXML
    private Text statusText;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        shipNumber.getItems().addAll(rc.getShipRepository().findAllShipsNumbers());
    }

    @Override
    public void onOK(ActionEvent event) {
        Ship ship = rc.getShipRepository().find(shipNumber.getSelectionModel().getSelectedItem());

        Order order = OrderBuilder.anOrder()
                .withOrderNumber(Integer.parseInt(orderNumber.getCharacters().toString()))
                .withOrderDescription(orderDescription.getCharacters().toString())
                .withShip(ship)
                .build();

        boolean respond = rc.getOrderRepository().create(order);
        if (respond) {
            statusText.setText("Zamówienie stworzone");
        } else {
            statusText.setText("Zamówienie nie zostało stworzone");
        }
    }
}
