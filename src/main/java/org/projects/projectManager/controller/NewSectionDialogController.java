package org.projects.projectManager.controller;

import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DialogPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import org.projects.projectManager.model.*;
import org.projects.projectManager.repository.RepositoryControl;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;


public class NewSectionDialogController extends ViewController implements Initializable {
    private final RepositoryControl rc;

    public NewSectionDialogController(RepositoryControl rc) {
        this.rc = rc;
    }


    @FXML
    private DialogPane dialogPane;

    @FXML
    private ComboBox<Integer> shipNumber;

    @FXML
    private TextArea drawingTitle;

    @FXML
    private TextField groupID;

    @FXML
    private TextField drawingID;

    @FXML
    private ComboBox<Integer> orderNumbers;

    @FXML
    private Text statusText;

    @FXML
    private TextField roomName;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        shipNumber.getItems().addAll(rc.getShipRepository().findAllShipsNumbers());
    }

    @FXML
    public void shipIDSelected() {
        orderNumbers.getSelectionModel().clearSelection();
        orderNumbers.getItems().clear();
        List<Order> orders = rc.getOrderRepository().findAllWithinShip(shipNumber.getSelectionModel().getSelectedItem());

        for (Order order : orders) {
            orderNumbers.getItems().add(order.getOrderId().getOrderNumber());
        }
    }




    @Override
    public void onOK(ActionEvent event) {
        Ship ship = rc.getShipRepository().find(shipNumber.getSelectionModel().getSelectedItem());
        Room room = rc.getRoomRepository().find(new RoomId(roomName.getText(), ship));
        if (room == null) {
            rc.getRoomRepository()
                    .create(RoomBuilder
                            .aRoom()
                            .withRoomNumber(roomName.getText())
                            .withShip(ship)
                            .build()
                    );
        }

        Section section = SectionBuilder.aSection()
                .withOrder(rc.getOrderRepository().find(new OrderId(orderNumbers.getSelectionModel().getSelectedItem(), ship)))
                .withGroupNumber(Integer.valueOf(groupID.getText()))
                .withDrawingNumber(drawingID.getText())
                .withTitle(drawingTitle.getText())
                .withRoom(rc.getRoomRepository().find(new RoomId(roomName.getText(), ship)))
                .build();

        rc.getSectionRepository().create(section);

        onCancel(event);
    }
}
