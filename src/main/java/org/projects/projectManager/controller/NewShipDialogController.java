package org.projects.projectManager.controller;

import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.DialogPane;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import org.projects.projectManager.model.Ship;
import org.projects.projectManager.model.ShipBuilder;
import org.projects.projectManager.repository.RepositoryControl;


public class NewShipDialogController extends ViewController {
    private final RepositoryControl rc;
    private final BasicWindowController dialogOwnerController;

    @FXML
    private DialogPane dialogPane;

    @FXML
    private TextField shipNumber;

    @FXML
    private TextField shipName;

    @FXML
    private Text statusText;

    public NewShipDialogController(RepositoryControl rc, BasicWindowController dialogOwnerController) {
        this.rc = rc;
        this.dialogOwnerController = dialogOwnerController;
    }

    @Override
    public void onOK(ActionEvent event) {
        Ship ship = ShipBuilder.aShip()
                .withShipNumber(Integer.parseInt(shipNumber.getCharacters().toString()))
                .withShipName(shipName.getCharacters().toString())
                .build();
        boolean respond = rc.getShipRepository().create(ship);
        if (respond) {
            statusText.setText("Statek stworzony");
            dialogOwnerController.update();
        } else {
            statusText.setText("Statek nie został stworzony");
        }
    }
}
