package org.projects.projectManager.controller;

import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import org.projects.projectManager.model.User;
import org.projects.projectManager.model.UserBuilder;
import org.projects.projectManager.repository.RepositoryControl;

public class NewUserDialogController extends ViewController {
    private final RepositoryControl rc;

    public NewUserDialogController(RepositoryControl rc) {
        this.rc = rc;
    }

    @FXML
    private TextField userName;

    @FXML
    private TextField userSurname;

    @FXML
    private TextField userInitials;

    @FXML
    private TextField userPrivilege;

    @FXML
    private Text statusText;

    @Override
    public void onOK(ActionEvent event) {
        User user = UserBuilder.anUser()
                .withInitials(userInitials.getCharacters().toString())
                .withName(userName.getCharacters().toString())
                .withSurname(userSurname.getCharacters().toString())
                .withPrivilege(Integer.parseInt(userPrivilege.getCharacters().toString()))
                .build();
        boolean respond = rc.getUserRepository().create(user);
        if (respond) {
            statusText.setText("Urzytkownik stworzony");
        } else {
            statusText.setText("Urzytkownik nie został stworzony");
        }
    }
}

