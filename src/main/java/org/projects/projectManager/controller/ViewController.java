package org.projects.projectManager.controller;

import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

public abstract class ViewController {
    @FXML
    public void onCancel(Event event) {
        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        stage.close();
    }

    @FXML
    public void onESC(KeyEvent event) {
        if (event.getCode() == KeyCode.ESCAPE) {
            onCancel(event);
        }
    }

    @FXML
    public abstract void onOK(ActionEvent event);
}
