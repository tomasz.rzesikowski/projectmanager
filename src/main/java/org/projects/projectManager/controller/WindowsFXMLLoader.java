package org.projects.projectManager.controller;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.layout.Pane;
import org.projects.projectManager.App;

import java.io.IOException;

public class WindowsFXMLLoader {

    private static Parent loadFXML(String FXMLFileName, ViewController viewController) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource(FXMLFileName + ".fxml"));
        fxmlLoader.setController(viewController);
        return fxmlLoader.load();
    }

//    public static Parent loadFXML(String FXMLFileName) {
//        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource(FXMLFileName + ".fxml"));
//        Pane pane = null;
//        try {
//            fxmlLoader.
//            pane = fxmlLoader.load();
//        } catch (IOException e) {
//            System.out.println("Nie można załadować okna");
//            e.printStackTrace();
//        }
//        return pane;
//    }

    public static void loadDialog(Pane windowOwner, String FXMLFileName, ViewController viewController) {

        Dialog<ButtonType> dialog = new Dialog<>();
        dialog.initOwner(windowOwner.getScene().getWindow());

        try {
            dialog.getDialogPane().setContent(loadFXML(FXMLFileName, viewController));
            dialog.showAndWait();
        } catch (Exception e){
            System.out.println("Nie można załadować okna");
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }

//    public static Scene loadScene(String FXMLFileName) {
//        Scene scene = null;
//        try {
//            FXMLLoader loader = new FXMLLoader();
//            Pane root = loader.load(new FileInputStream(getFxmlFilesFolder(FXMLFileName)));
////            scene = new Scene(root);
////            scene = new Scene()
//            scene = new Scene(root);
////            scene = loader.load(new FileInputStream(getFxmlFilesFolder(FXMLFileName)));
//
//        } catch (Exception e){
//            System.out.println("Nie można załadować okna");
//            System.out.println(e.getMessage());
//            e.printStackTrace();
//        }
//        return scene;
//    }

}
