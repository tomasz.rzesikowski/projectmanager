package org.projects.projectManager.entity;

import lombok.AllArgsConstructor;
import lombok.experimental.Delegate;

import javax.persistence.EntityManager;

@AllArgsConstructor
public class CloseableEntityManager implements EntityManager, AutoCloseable {

    @Delegate
    private final EntityManager em;
}
