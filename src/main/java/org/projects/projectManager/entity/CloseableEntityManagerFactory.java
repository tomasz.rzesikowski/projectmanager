package org.projects.projectManager.entity;

import lombok.AllArgsConstructor;

import javax.persistence.EntityManagerFactory;

@AllArgsConstructor
public class CloseableEntityManagerFactory implements AutoCloseable {

    private final EntityManagerFactory emf;

    public CloseableEntityManager createEntityManager() {
        return new CloseableEntityManager(emf.createEntityManager());
    }

    @Override
    public void close() {
        emf.close();
    }
}
