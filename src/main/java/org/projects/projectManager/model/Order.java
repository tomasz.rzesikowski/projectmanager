package org.projects.projectManager.model;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@NoArgsConstructor
@EqualsAndHashCode(exclude = {"sections"})
@ToString(exclude = {"sections"})
@Entity
@Table(name="orders")
public class Order {

    @EmbeddedId
    @Getter @Setter
    private OrderId orderId;

    @Column(name="order_description")
    @Getter @Setter
    private String orderDescription;
    /*
    Relation one to Many between Orders and Sections
    */
    @OneToMany(mappedBy = "order")
    @Getter
    private List<Section> sections;
}
