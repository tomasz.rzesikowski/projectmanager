package org.projects.projectManager.model;

public final class OrderBuilder {
    private int orderNumber;
    private String orderDescription;
    private Ship ship;

    private OrderBuilder() {
    }

    public static OrderBuilder anOrder() {
        return new OrderBuilder();
    }

    public OrderBuilder withOrderNumber(int orderNumber) {
        this.orderNumber = orderNumber;
        return this;
    }

    public OrderBuilder withOrderDescription(String orderDescription) {
        this.orderDescription = orderDescription;
        return this;
    }

    public OrderBuilder withShip(Ship ship) {
        this.ship = ship;
        return this;
    }


    public Order build() {
        Order order = new Order();
        OrderId orderId = new OrderId();
        orderId.setOrderNumber(orderNumber);
        orderId.setShip(ship);
        order.setOrderId(orderId);
        order.setOrderDescription(orderDescription);
        return order;
    }
}
