package org.projects.projectManager.model;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
@Embeddable
public class OrderId implements Serializable {
    static final long serialVersionUID = 1111L;

    @Column(name="order_number")
    @Getter @Setter
    private int orderNumber;

    /*
    Relation many to one between Orders and Ships
    */
    @ManyToOne
    @Getter @Setter
    private Ship ship;
}
