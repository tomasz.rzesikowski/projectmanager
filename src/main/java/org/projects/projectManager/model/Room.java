package org.projects.projectManager.model;

import lombok.*;

import javax.persistence.*;
import java.util.List;


@NoArgsConstructor
@EqualsAndHashCode(exclude = {"sections"})
@ToString(exclude = {"sections"})
@Entity
@Table(name = "rooms")
public class Room {

    @EmbeddedId
    @Getter @Setter
    private RoomId roomId;

    @Getter @Setter
    @Column(name = "correspondend_drawing")
    private String correspondentDrawing;

    @OneToMany(mappedBy = "room")
    @Getter @Setter
    private List<Section> sections;
}
