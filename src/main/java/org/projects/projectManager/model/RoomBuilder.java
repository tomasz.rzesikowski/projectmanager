package org.projects.projectManager.model;

import java.math.RoundingMode;

public final class RoomBuilder {
    private String roomNumber;
    private Ship ship;
    private String correspondentDrawing;

    private RoomBuilder() {
    }

    public static RoomBuilder aRoom() {
        return new RoomBuilder();
    }

    public RoomBuilder withRoomNumber(String roomNumber) {
        this.roomNumber = roomNumber;
        return this;
    }

    public RoomBuilder withShip(Ship ship) {
        this.ship = ship;
        return this;
    }

    public RoomBuilder withCorrespondentDrawing(String correspondentDrawing) {
        this.correspondentDrawing = correspondentDrawing;
        return this;
    }

    public Room build() {
        Room room = new Room();
        RoomId roomId = new RoomId();
        roomId.setRoomNumber(roomNumber);
        roomId.setShip(ship);
        room.setRoomId(roomId);
        room.setCorrespondentDrawing(correspondentDrawing);
        return room;
    }
}
