package org.projects.projectManager.model;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
@Embeddable
public class RoomId implements Serializable {
    static final long serialVersionUID = 1112L;

    @Column(name = "room_number")
    @Getter @Setter
    private String roomNumber;

    @Getter @Setter
    @ManyToOne
    private Ship ship;
}
