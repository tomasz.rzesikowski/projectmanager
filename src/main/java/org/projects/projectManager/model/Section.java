package org.projects.projectManager.model;

import lombok.*;

import javax.persistence.*;

@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Entity
@Table(name = "sections")
public class Section {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    @Getter @Setter
    private Integer id;

    @Getter @Setter
    private String title;

    @Column(name = "group_number")
    @Getter @Setter
    private Integer groupNumber;

    @Column(name = "drawing_number")
    @Getter @Setter
    private String drawingNumber;

    @ManyToOne
    @Setter
    private Room room;

    @ManyToOne
    @Getter @Setter
    private Order order;

    public String getRoom() {
        return room.getRoomId().getRoomNumber();
    }

    public Integer getShipNumber() {
        return room.getRoomId().getShip().getShipNumber();
    }

    public String getSectionNumber() {
        return groupNumber + "-" + drawingNumber;
    }
}
