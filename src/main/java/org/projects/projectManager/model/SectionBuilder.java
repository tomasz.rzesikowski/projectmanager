package org.projects.projectManager.model;

public final class SectionBuilder {
    private String title;
    private Integer groupNumber;
    private String drawingNumber;
    private Room room;
    private Order order;

    private SectionBuilder() {
    }

    public static SectionBuilder aSection() {
        return new SectionBuilder();
    }

    public SectionBuilder withTitle(String title) {
        this.title = title;
        return this;
    }

    public SectionBuilder withGroupNumber(Integer groupNumber) {
        this.groupNumber = groupNumber;
        return this;
    }

    public SectionBuilder withDrawingNumber(String drawingNumber) {
        this.drawingNumber = drawingNumber;
        return this;
    }

    public SectionBuilder withRoom(Room room) {
        this.room = room;
        return this;
    }

    public SectionBuilder withOrder(Order order) {
        this.order = order;
        return this;
    }

    public Section build() {
        Section section = new Section();
        section.setTitle(title);
        section.setGroupNumber(groupNumber);
        section.setDrawingNumber(drawingNumber);
        section.setRoom(room);
        section.setOrder(order);
        return section;
    }
}
