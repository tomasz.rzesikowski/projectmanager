package org.projects.projectManager.model;


import lombok.*;

import javax.persistence.*;
import java.util.List;

@NoArgsConstructor
@EqualsAndHashCode(exclude = {"rooms"})
@ToString(exclude = {"rooms"})
@Entity
@Table(name = "ships")
public class Ship {

    @Id
    @Column(name = "ship_number")
    @Getter @Setter
    private Integer shipNumber;

    @Column(name = "ship_name")
    @Getter @Setter
    private String shipName;

    @OneToMany(mappedBy = "roomId.ship")
    @Getter @Setter
    private List<Room> rooms;
}
