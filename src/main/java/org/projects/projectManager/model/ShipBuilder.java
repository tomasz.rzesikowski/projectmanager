package org.projects.projectManager.model;

public final class ShipBuilder {
    private Integer shipNumber;
    private String shipName;

    private ShipBuilder() {
    }

    public static ShipBuilder aShip() {
        return new ShipBuilder();
    }

    public ShipBuilder withShipNumber(Integer shipNumber) {
        this.shipNumber = shipNumber;
        return this;
    }

    public ShipBuilder withShipName(String shipName) {
        this.shipName = shipName;
        return this;
    }

    public Ship build() {
        Ship ship = new Ship();
        ship.setShipNumber(shipNumber);
        ship.setShipName(shipName);
        return ship;
    }
}
