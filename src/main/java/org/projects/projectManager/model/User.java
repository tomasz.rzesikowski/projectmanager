package org.projects.projectManager.model;

import lombok.*;

import javax.persistence.*;

@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Entity
@Table(name = "users")
public class User {

    @Id
    @Getter @Setter
    private String initials;

    @Getter @Setter
    private String name;

    @Getter @Setter
    private String surname;

    @Getter @Setter
    private Integer privilege;

    public String getFullName() {
        return name + " " + surname;
    }
}
