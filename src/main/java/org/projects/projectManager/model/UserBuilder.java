package org.projects.projectManager.model;

public final class UserBuilder {
    private String initials;
    private String name;
    private String surname;
    private Integer privilege = 1;

    private UserBuilder() {
    }

    public static UserBuilder anUser() {
        return new UserBuilder();
    }

    public UserBuilder withInitials(String initials) {
        this.initials = initials;
        return this;
    }

    public UserBuilder withName(String name) {
        this.name = name;
        return this;
    }

    public UserBuilder withSurname(String surname) {
        this.surname = surname;
        return this;
    }

    public UserBuilder withPrivilege(Integer privilege) {
        this.privilege = privilege;
        return this;
    }

    public User build() {
        User user = new User();
        user.setInitials(initials);
        user.setName(name);
        user.setSurname(surname);
        user.setPrivilege(privilege);
        return user;
    }
}
