package org.projects.projectManager.repository;

import lombok.AllArgsConstructor;
import org.projects.projectManager.entity.CloseableEntityManager;
import org.projects.projectManager.entity.CloseableEntityManagerFactory;

import javax.persistence.RollbackException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.function.Function;

@AllArgsConstructor
public abstract class BaseRepository<T, E> {

    protected abstract Class<T> getEntityClass();

    protected abstract Function<T, E> getKeyExtractor();

    protected final CloseableEntityManagerFactory factory;

    public T find(Object primaryKey) {
        try (CloseableEntityManager em = factory.createEntityManager()) {
            return em.find(getEntityClass(), primaryKey);
        }
    }

    public boolean create(T entity) {
        try (CloseableEntityManager em = factory.createEntityManager()) {
            em.getTransaction().begin();
            em.persist(entity);
            em.getTransaction().commit();
        } catch (RollbackException e) {
            return false;
        }
        return true;
    }

    public T update(T entity) {
        try (CloseableEntityManager em = factory.createEntityManager()) {
            em.getTransaction().begin();
            entity = em.merge(entity);
            em.getTransaction().commit();
            return entity;
        }
    }

    public void remove(T entity) {
        try (CloseableEntityManager em = factory.createEntityManager()) {
            em.getTransaction().begin();
            em.remove(em.find(getEntityClass(), getKeyExtractor().apply(entity)));
            em.getTransaction().commit();
        }
    }

    public List<T> findAll() {
        try (CloseableEntityManager em = factory.createEntityManager()) {
            CriteriaBuilder builder = em.getCriteriaBuilder();
            CriteriaQuery<T> query = builder.createQuery(getEntityClass());
            Root<T> root = query.from(getEntityClass());
            query.select(root);
            return em.createQuery(query).getResultList();
        }
    }
}
