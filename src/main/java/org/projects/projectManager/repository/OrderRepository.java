package org.projects.projectManager.repository;


import org.projects.projectManager.entity.CloseableEntityManager;
import org.projects.projectManager.entity.CloseableEntityManagerFactory;
import org.projects.projectManager.model.Order;
import org.projects.projectManager.model.OrderId;
import org.projects.projectManager.model.Order_;
import org.projects.projectManager.model.Section;
import org.projects.projectManager.repository.query.OrderCriteriaQueries;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.function.Function;


public class OrderRepository extends BaseRepository<Order, OrderId> {
    private final OrderCriteriaQueries queries;

    public OrderRepository(CloseableEntityManagerFactory factory, OrderCriteriaQueries queries) {
        super(factory);
        this.queries = queries;
    }

    @Override
    protected Class<Order> getEntityClass() {
        return Order.class;
    }

    @Override
    protected Function<Order, OrderId> getKeyExtractor() {
        return Order::getOrderId;
    }

    public List<Order> findAllWithinShip(Integer shipNumber) {
        try (CloseableEntityManager em = factory.createEntityManager()) {
            return queries.findAllWithinShip(em, shipNumber);
        }
    }

    public List<Section> findAllSectionsWithShipAndOrder (Integer shipNumber, Integer orderNumber) {
        try (CloseableEntityManager em = factory.createEntityManager()) {
            return queries.findAllSectionsWithShipAndOrder(em, shipNumber, orderNumber);
        }
    }


}
