package org.projects.projectManager.repository;

import lombok.Getter;
import org.projects.projectManager.entity.CloseableEntityManagerFactory;
import org.projects.projectManager.repository.query.OrderCriteriaQueries;
import org.projects.projectManager.repository.query.ShipCriteriaQueries;

@Getter
public class RepositoryControl {
    private final UserRepository userRepository;
    private final ShipRepository shipRepository;
    private final SectionRepository sectionRepository;
    private final RoomRepository roomRepository;
    private final OrderRepository orderRepository;


    public RepositoryControl(CloseableEntityManagerFactory emf) {
        this.userRepository = new UserRepository(emf);
        this.shipRepository = new ShipRepository(emf , new ShipCriteriaQueries());
        this.sectionRepository = new SectionRepository(emf);
        this.roomRepository = new RoomRepository(emf);
        this.orderRepository = new OrderRepository(emf , new OrderCriteriaQueries());
    }
}
