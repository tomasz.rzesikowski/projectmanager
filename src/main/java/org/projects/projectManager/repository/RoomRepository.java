package org.projects.projectManager.repository;


import org.projects.projectManager.entity.CloseableEntityManagerFactory;
import org.projects.projectManager.model.Room;
import org.projects.projectManager.model.RoomId;
import org.projects.projectManager.model.Ship;

import java.util.function.Function;

public class RoomRepository extends BaseRepository<Room, RoomId> {

    public RoomRepository(CloseableEntityManagerFactory factory) {
        super(factory);
    }

    @Override
    protected Class<Room> getEntityClass() {
        return Room.class;
    }

    @Override
    protected Function<Room, RoomId> getKeyExtractor() {
        return Room::getRoomId;
    }

}
