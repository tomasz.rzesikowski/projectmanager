package org.projects.projectManager.repository;


import org.projects.projectManager.entity.CloseableEntityManagerFactory;
import org.projects.projectManager.model.Section;

import java.util.function.Function;

public class SectionRepository extends BaseRepository<Section, Integer> {

    public SectionRepository(CloseableEntityManagerFactory factory) {
        super(factory);
    }

    @Override
    protected Class<Section> getEntityClass() {
        return Section.class;
    }

    @Override
    protected Function<Section, Integer> getKeyExtractor() {
        return Section::getId;
    }

}
