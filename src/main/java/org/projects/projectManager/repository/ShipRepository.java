package org.projects.projectManager.repository;


import org.projects.projectManager.entity.CloseableEntityManagerFactory;
import org.projects.projectManager.model.Ship;
import org.projects.projectManager.repository.query.OrderCriteriaQueries;
import org.projects.projectManager.repository.query.ShipCriteriaQueries;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ShipRepository extends BaseRepository<Ship, Integer> {
    private final ShipCriteriaQueries queries;

    public ShipRepository(CloseableEntityManagerFactory factory, ShipCriteriaQueries queries) {
        super(factory);
        this.queries = queries;
    }

    @Override
    protected Class<Ship> getEntityClass() {
        return Ship.class;
    }

    @Override
    protected Function<Ship, Integer> getKeyExtractor() {
        return Ship::getShipNumber;
    }

    public List<Integer> findAllShipsNumbers() {
        return this.findAll()
                .stream()
                .map(Ship::getShipNumber)
                .collect(Collectors.toList());
    }
}
