package org.projects.projectManager.repository;


import org.projects.projectManager.entity.CloseableEntityManagerFactory;
import org.projects.projectManager.model.User;

import java.util.function.Function;

public class UserRepository extends BaseRepository<User, String> {

    public UserRepository(CloseableEntityManagerFactory factory) {
        super(factory);
    }

    @Override
    protected Class<User> getEntityClass() {
        return User.class;
    }

    @Override
    protected Function<User, String> getKeyExtractor() {
        return User::getInitials;
    }

}
