package org.projects.projectManager.repository.query;

import org.projects.projectManager.model.Order;
import org.projects.projectManager.model.OrderId_;
import org.projects.projectManager.model.Order_;
import org.projects.projectManager.model.Section;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public class OrderCriteriaQueries {

    public List<Order> findAllWithinShip (EntityManager em, Integer shipNumber) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Order> query = cb.createQuery(Order.class);
        Root<Order> root = query.from(Order.class);
        query.select(root).where(cb.equal(root.get(Order_.orderId).get(OrderId_.ship), shipNumber));
        return em.createQuery(query).getResultList();
    }

    public List<Section> findAllSectionsWithShipAndOrder (EntityManager em, Integer shipNumber, Integer orderNumber) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Section> query = cb.createQuery(Section.class);
        Root<Order> root = query.from(Order.class);
        query.select(root.get("sections"))
                .where(cb.equal(root.get(Order_.orderId).get(OrderId_.ship), shipNumber))
                .where(cb.equal(root.get(Order_.orderId).get(OrderId_.orderNumber), orderNumber));
        return em.createQuery(query).getResultList();
    }
}
